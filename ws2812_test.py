import ws2812
import time
import math

def alternate(top):
    for x in xrange(top):
        yield top * 2 - x
        yield x

writer = ws2812.PixelWriter()
pixels = []

BGR_gradient = [[0,0,255], [255, 0, 255], [255,0,0],[255,255,0],
                [0,255,0]]

for x in xrange(30):
    pixels.append(ws2812.map_float_to_rgb(x/255.0,BGR_gradient))
 
while True:
    for brightness in xrange(0,255,2):
        pixels.append(ws2812.map_float_to_rgb(brightness/255.0, BGR_gradient))
        pixels = pixels[1:]
        writer.write(pixels)
