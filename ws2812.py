import math
import serial
import time
import struct

def map_float_to_rgb(value, color_gradient):
    color_index = value*(len(color_gradient)-1)
    color_lower = int(math.floor(color_index))
    color_upper = int(math.ceil(color_index))
    if(color_lower == color_upper):
        return color_gradient[int(color_lower)]
    else:
        color = []
        for i in range(3):
            color.append(int(color_gradient[color_lower][i] + 
                (color_index - color_lower)*
                (color_gradient[color_upper][i]
                 -color_gradient[color_lower][i])))
        return color

class PixelWriter(object):
    def __init__(self):
        self.serial = serial.Serial()
        self.serial.port = "/dev/ttyACM0"
        self.serial.baudrate = 9600
        self.serial.bytesize = serial.EIGHTBITS
        self.serial.parity = serial.PARITY_NONE
        self.serial.stopbits = serial.STOPBITS_ONE
        self.serial.open()

    def write(self, pixels):
        # TODO: replace this homebrew packing
        frame = [0xCC, 0xAA, 0xFF, 0xEE, len(pixels)*3 & 0xFF, (len(pixels)*3 & 0xFF00) >> 8]
        for pixel in pixels:
            frame.extend(pixel)

        self.serial.write(frame)
        #result = self.serial.readline()
        #print len(result)
        #print struct.unpack('62B',result)
