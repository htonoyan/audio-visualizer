import argparse
import numpy
from matplotlib import pyplot
from matplotlib import animation
import audio

parser = argparse.ArgumentParser(description='Audio visualizer.')
parser.add_argument('--fft', type=int, default=1024, help='FFT Length for specturm analyzer mode')
parser.add_argument('--bars_per_oct', type=int, default=8, help='Number of bars to display per octave')
parser.add_argument('--chunk', type=int, default=1024, help='Number of audio samples to read')
args = parser.parse_args()

########################################################################
def plot_animation(audio_stream, fft_length, bars_per_oct):
    drawer = DrawFunctions(audio_stream=audio_stream,
                           fft_length=fft_length,
                           bars_per_oct=bars_per_oct)
    drawer.start_animation()

class DrawFunctions:
    def __init__(self, audio_stream, fft_length,
                 bars_per_oct):
        self.audio_stream = audio_stream
        self.fft_length = fft_length
        self.fig = pyplot.figure()
        self.bars_per_oct = bars_per_oct

        self.num_octaves = int(numpy.log2(fft_length))
        self.num_bars = self.num_octaves * self.bars_per_oct

        ax = pyplot.axes(xlim=(0, self.num_bars), ylim=(1, 2**22))

        self.bars = ax.bar(range(self.num_bars),
                           [1 for i in range(self.num_bars)],
                           width=1,
                           log=True)

    def start_animation(self):
        ani = animation.FuncAnimation(self.fig,
                                      self.update,
                                      interval=10,
                                      blit=True)
        pyplot.show()

    def update(self,i):
        frames = self.audio_stream.read_audio()
        spectrum = numpy.absolute(numpy.fft.rfft(frames,
                                                 n=self.fft_length))
        for i in range(self.num_bars):
            self.bars[i].set_height(sum(spectrum[
                round(2**(float(i)/self.bars_per_oct),0):
                round(2**(float(i+1)/self.bars_per_oct),0)+1]))
            print round(2**(float(i)/self.bars_per_oct),0), round(2**(float(i+1)/self.bars_per_oct),0)+1
        return self.bars

stream = audio.audio_input(args.chunk)
plot_animation(stream, args.fft, args.bars_per_oct)
