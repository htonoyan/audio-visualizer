import argparse
import ws2812
import audio
import math
import time
import sounddevice as sd
import numpy as np
print "start"

parser = argparse.ArgumentParser(description='Audio visualizer.')
parser.add_argument('--LEDs', type=int, default=20, help='Number of LEDs to use in light show')
parser.add_argument('--chunk', type=int, default=1024, help='Audio chunk size')
args = parser.parse_args()

BGR_gradient = [[0,0,255], [255, 0, 255], [255,0,0],[255,255,0],
                [0,255,0]]
#BGR_gradient = [[255, 0, 0], [255, 0, 255], [0, 255, 0]]

pixels = [[0,0,0]]*args.LEDs
pixel_brightnesses = [0]*args.LEDs
output_buffer = [[0,0,0]]*args.LEDs

pixel_output = ws2812.PixelWriter()
print "pixelwriter"

def audio_callback(frames, num_frames, time, status):
    global pixels
    global pixel_output
    global pixel_brightnesses
    global output_buffer
    peak = max(frames)
    rms = np.sqrt(np.mean(frames**2))
#    print "%f, %f, %f" % (peak, min(frames), rms)

    if rms > 1e-6:
        pixel_color = ws2812.map_float_to_rgb(peak, BGR_gradient)
        pixel_brightnesses = [1.0]*args.LEDs
    else:
        pixel_color = ws2812.map_float_to_rgb(0.1, BGR_gradient)
        for i in xrange(len(pixel_brightnesses)):
            if pixel_brightnesses[i] > 0.1:
                pixel_brightnesses[i] -= 0.1
            else:
                pixel_brightnesses[i] = 0

    pixels.insert(0, pixel_color)
    pixels.pop()

    for i in xrange(len(pixel_brightnesses)):
        output_buffer[i] = [int(pixel_brightnesses[i]*color) for color in pixels[i]]

    pixel_output.write(output_buffer[::-1]+output_buffer)
#    print output_buffer

device_info = sd.query_devices(kind='input')
samplerate = device_info['default_samplerate']
stream = sd.InputStream(callback=audio_callback, channels=1, blocksize=args.chunk)
with stream:
    while True:
        time.sleep(1)
