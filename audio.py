import numpy as np
import argparse

import sounddevice as sd
########################################################################

class audio_input:
    def __init__(self, chunk_size, audio_callback):
        self.device_info = sd.query_devices(kind='input')
        self.samplerate = self.device_info['default_samplerate']
        self.chunk_size = chunk_size
        self.stream = sd.InputStream(callback=audio_callback, channels=1, blocksize=chunk_size)

    def read_audio(self):
        def get_frames():
            available_frames = self.stream.get_read_available()
            print available_frames
            if available_frames >= self.chunk_size:
                return self.stream.read(available_frames)
        frames = get_frames()
        while not frames:
            frames = get_frames()
        
        return np.fromstring(frames, self.dt)['left'][-self.chunk_size:]
